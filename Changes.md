## Changes made by Dennis for custom endorsement logic testing
1. Created `fabric/core/handlers/endorsement/plugin/escc.so` and copied to `fabric-samples/plugin/endorsement/escc.so`

2. Created `fabric-samples/config/core-test.yaml` and updated escc library

Original
```yaml
endorsers:
  escc:
    name: DefaultEndorsement
    library:
```

Current
```yaml
endorsers:
  escc:
    name: DefaultEndorsement
    library: /etc/hyperledger/fabric/plugin/escc.so
```

3. Update `fabric-samples/test-network/docker/docker-compose-test-net.yaml`
    - peer0.org1.example.com.volumes
    - peer0.org2.example.com.volumes

Added the following lines
```
- ../../plugin/endorsement/escc.so:/etc/hyperledger/fabric/plugin/escc.so
- ../../config/core-test.yaml:/etc/hyperledger/fabric/core.yaml
```

To use the default endorsement logic, comment the following lines (83, 123) in the `docker-compose-test-net.yaml`.
```
- ../../config/core-test.yaml:/etc/hyperledger/fabric/core.yaml
```

## Problems
1. peer failed to join channel after using escc.so (library specified in core-test.yaml), it works fine if we remove `/etc/hyperledger/fabric/plugin/escc.so` in `core-test.yaml`. Peer node failed to open the escc plugin. Tried different golang version, peer node still unable to open the plugin.