VERSION=$1

echo ${VERSION}

docker run --rm -v "$PWD":/app -w /app/core/handlers/endorsement/plugin amd64/golang:1.14.${VERSION}-buster go build -buildmode=plugin -o escc-1_14_${VERSION}-buster_amd64.so plugin.go;

docker run --rm -v "$PWD":/app -w /app/core/handlers/endorsement/plugin amd64/golang:1.14.${VERSION}-stretch go build -buildmode=plugin -o escc-1_14_${VERSION}-stretch_amd64.so plugin.go;

docker run --rm -v "$PWD":/app -w /app/core/handlers/endorsement/plugin amd64/golang:1.14.${VERSION} go build -buildmode=plugin -o escc-1_14_${VERSION}_amd64.so plugin.go;

docker run --rm -v "$PWD":/app -w /app amd64/golang:1.14.${VERSION}-alpine3.11 /bin/sh -c "apk add --no-cache \
	bash \
	gcc \
	git \
	make \
	musl-dev && go version && go mod download && cd /app/core/handlers/endorsement/plugin && go build -buildmode=plugin -o escc-1_14_${VERSION}_alpine_3_11_amd64.so plugin.go";

docker run --rm -v "$PWD":/app -w /app amd64/golang:1.14.${VERSION}-alpine3.12 /bin/sh -c "apk add --no-cache \
	bash \
	gcc \
	git \
	make \
	musl-dev && go version && go mod download && cd /app/core/handlers/endorsement/plugin && go build -buildmode=plugin -o escc-1_14_${VERSION}_alpine_3_12_amd64.so plugin.go";

docker run --rm -v "$PWD":/app -w /app amd64/golang:1.14.${VERSION}-alpine /bin/sh -c "apk add --no-cache \
	bash \
	gcc \
	git \
	make \
	musl-dev && go version && go mod download && cd /app/core/handlers/endorsement/plugin && go build -buildmode=plugin -o escc-1_14_${VERSION}_alpine_amd64.so plugin.go";